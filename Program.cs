﻿using homework_multithreading.src;
using System;
using System.Diagnostics;

namespace homework_multithreading
{
    class Program
    {
        static int CASE_1 = 100_000;
        static int CASE_2 = 1_000_000;
        static int CASE_3 = 10_000_000;

        static void Main(string[] args)
        {
            // Prepare data
            DataGenerator dataGenerator = new DataGenerator();

            int[] data1 = dataGenerator.GenerateData(CASE_1);
            int[] data2 = dataGenerator.GenerateData(CASE_2);
            int[] data3 = dataGenerator.GenerateData(CASE_3);


            // 1) Sequential execution
            SequentialExecution sequentialExecution = new SequentialExecution();

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();
            var c11 = sequentialExecution.Execute(data1);
            stopwatch.Stop();
            Console.WriteLine($"Sequential execution, case 1 (ticks / ms): {stopwatch.ElapsedTicks} / {stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond}");

            stopwatch.Reset();
            stopwatch.Start();
            var c12 = sequentialExecution.Execute(data2);
            stopwatch.Stop();
            Console.WriteLine($"Sequential execution, case 2 (ticks / ms): {stopwatch.ElapsedTicks} / {stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond}");

            stopwatch.Reset();
            stopwatch.Start();
            var c13 = sequentialExecution.Execute(data3);
            stopwatch.Stop();
            Console.WriteLine($"Sequential execution, case 3 (ticks / ms): {stopwatch.ElapsedTicks} / {stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond}");

            // 2) Parallel execution
            const int NUMBER_OF_THREADS = 10;
            ParallelExecution parallelExecution1 = new ParallelExecution(NUMBER_OF_THREADS);

            stopwatch.Reset();
            stopwatch.Start();
            var c21 = parallelExecution1.Execute(data1);
            stopwatch.Stop();
            Console.WriteLine($"Parallel execution, case 1 (# threads: {parallelExecution1.NumberOfThreads}) (ticks / ms): {stopwatch.ElapsedTicks} / {stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond}");

            ParallelExecution parallelExecution2 = new ParallelExecution(NUMBER_OF_THREADS);

            stopwatch.Reset();
            stopwatch.Start();
            var c22 = parallelExecution2.Execute(data2);
            stopwatch.Stop();
            Console.WriteLine($"Parallel execution, case 2 (# threads: {parallelExecution2.NumberOfThreads}) (ticks / ms): {stopwatch.ElapsedTicks} / {stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond}");

            ParallelExecution parallelExecution3 = new ParallelExecution(NUMBER_OF_THREADS);

            stopwatch.Reset();
            stopwatch.Start();
            var c23 = parallelExecution3.Execute(data3);
            stopwatch.Stop();
            Console.WriteLine($"Parallel execution, case 3 (# threads: {parallelExecution3.NumberOfThreads}) (ticks / ms): {stopwatch.ElapsedTicks} / {stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond}");


            // 3) PLinq
            ParallelLinq parallelLinq = new ParallelLinq();

            stopwatch.Reset();
            stopwatch.Start();
            var c31 = parallelLinq.Execute(data1);
            stopwatch.Stop();
            Console.WriteLine($"PLinq execution, case 1 (ticks / ms): {stopwatch.ElapsedTicks} / {stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond}");

            stopwatch.Reset();
            stopwatch.Start();
            var c32 = parallelLinq.Execute(data2);
            stopwatch.Stop();
            Console.WriteLine($"PLinq execution, case 2 (ticks / ms): {stopwatch.ElapsedTicks} / {stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond}");

            stopwatch.Reset();
            stopwatch.Start();
            var c33 = parallelLinq.Execute(data3);
            stopwatch.Stop();
            Console.WriteLine($"PLinq execution, case 3 (ticks / ms): {stopwatch.ElapsedTicks} / {stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond}");

            Console.WriteLine("=== REVALIDATE ===");
            Console.WriteLine($"Case 1 values are equal: {c11 == c21 && c11 == c31}, value: {c11}");
            Console.WriteLine($"Case 2 values are equal: {c12 == c22 && c12 == c32}, value: {c12}");
            Console.WriteLine($"Case 3 values are equal: {c13 == c23 && c13 == c33}, value: {c13}");
        }
    }
}
