﻿using homework_multithreading.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace homework_multithreading.src
{
    class SequentialExecution : IExecutor
    {
        public double Execute(int[] elements)
        {
            double sum = 0;

            foreach (var elem in elements)
            {
                sum += elem;
            }

            return sum;
        }
    }
}
