﻿using homework_multithreading.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace homework_multithreading.src
{
    class DataGenerator : IDataGenerator
    {
        public int[] GenerateData(int numberOfElements)
        {
            Random rand = new Random();
            int[] draftData = new int[numberOfElements];

            for (var i = 0; i < draftData.Length; i++)
            {
                draftData[i] = rand.Next(-10000, 10000);
            }

            return draftData;
        }
    }
}
