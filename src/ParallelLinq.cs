﻿using homework_multithreading.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace homework_multithreading.src
{
    class ParallelLinq : IExecutor
    {
        public double Execute(int[] elements)
        {
            return elements.AsParallel().Aggregate(
                0,
                (subtotal, item) => subtotal + item,
                (total, thisThread) => total + thisThread,
                (finalSum) => finalSum
            );
        }
    }
}
