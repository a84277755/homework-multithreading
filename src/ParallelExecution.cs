﻿using homework_multithreading.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace homework_multithreading.src
{
    class ParallelExecution : IExecutor
    {
        private int numberOfThreads;
        private object _lock;
        private Barrier _barrier;

        public ParallelExecution(int threadsNumber = 5)
        {
            numberOfThreads = threadsNumber;
            _lock = new object();
            _barrier = new Barrier(numberOfThreads);
        }

        public int NumberOfThreads
        {
            get { return numberOfThreads; }
        }

        public double Execute(int[] elements)
        {            
            double sum = 0;

            int stepSize = (int)(elements.Length / numberOfThreads);

            for (int i = 0; i < numberOfThreads; i++)
            {
                int leftIndex = i * stepSize;
                int rightIndex = (i + 1) * stepSize;
                Thread thread = new Thread(() => GetSum(elements, leftIndex, rightIndex, ref sum));
                thread.Start();
            }

            _barrier.AddParticipant();
            _barrier.SignalAndWait();

            return sum;
        }

        private void GetSum(int[] elements, int leftIndex, int rightIndex, ref double externalSum)
        {
            double sum = 0;

            while (leftIndex < rightIndex)
            {
                try
                {
                    sum += elements[leftIndex++];
                } catch
                {
                    break;
                }
            }

            lock(_lock)
            {
                externalSum += sum;
            }

            _barrier.SignalAndWait();
        }
    }
}
