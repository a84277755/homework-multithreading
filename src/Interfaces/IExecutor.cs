﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework_multithreading.src.Interfaces
{
    interface IExecutor
    {
        public double Execute(int[] elements);
    }
}
