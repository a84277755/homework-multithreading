﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework_multithreading.src.Interfaces
{
    interface IDataGenerator
    {
        public int[] GenerateData(int numberOfElements);
    }
}
